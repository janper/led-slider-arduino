#include <Stepper.h>

// ███████╗██╗     ██╗██████╗ ███████╗██████╗
// ██╔════╝██║     ██║██╔══██╗██╔════╝██╔══██╗
// ███████╗██║     ██║██║  ██║█████╗  ██████╔╝
// ╚════██║██║     ██║██║  ██║██╔══╝  ██╔══██╗
// ███████║███████╗██║██████╔╝███████╗██║  ██║
// ╚══════╝╚══════╝╚═╝╚═════╝ ╚══════╝╚═╝  ╚═╝

#define RELATIVE_COLLISION_BUFFER 100

// Represents a slider powered by a motor, with collision switches on both ends.
class Slider
{
private:
    // Collision switches
    int _leftSwitchPin;
    int _rightSwitchPin;
    // Position to reach by calling update()
    int _targetPosition;
    // Current calculated slider position (physical position might differ due to
    // drifts and gear grinding)
    int _position;
    // Number of steps to move the slider from one end to another.
    int _sliderRange;
    // True if the minimum and the maximum for the slider has been calibrated.
    // If false, all position indicators are faulty.
    bool _calibrated;
    // Number of steps to make in one iteration (doesn't need to be a whole
    // number, the update() function will compensate)
    float _stepLength;
    // Amount of steps missed in the previous iteration (should be < 1.0)
    float _remainder;
    // The physical motor
    Stepper _motor;
    bool _collisionSwitchesReleased();
    bool _leftCollisionSwitchPressed();
    bool _rightCollisionSwitchPressed();
    bool _calibrationDirection;

public:
    Slider(
        int stepsPerRevolution,
        int motorPin1, int motorPin2, int motorPin3, int motorPin4,
        int leftCollisionSwitchPin, int rightCollisionSwitchPin);
    void setTargetPosition(int position, float step);
    int position();
    int targetPosition();
    float stepLength();
    int sliderRange();
    bool isAtTarget();
    bool isCalibrated();
    void requestCalibration();
    void update();
    void calibrationUpdate();
};

Slider::Slider(
    int stepsPerRevolution,
    int motorPin1, int motorPin2, int motorPin3, int motorPin4,
    int leftCollisionSwitchPin, int rightCollisionSwitchPin)
    : _motor(stepsPerRevolution, motorPin1, motorPin2, motorPin3, motorPin4)
{
    _leftSwitchPin = leftCollisionSwitchPin;
    _rightSwitchPin = rightCollisionSwitchPin;
    _calibrated = false;
    _calibrationDirection = false;
    _position = 0;
    _targetPosition = 0;
    _sliderRange = 0;
    pinMode(_leftSwitchPin, INPUT);
    pinMode(_rightSwitchPin, INPUT);
    _motor.setSpeed(15);
}

// Request slider calibration. Enables calibrationUpdate().
void Slider::requestCalibration()
{
    _calibrated = false;
}

// True if no collision switch is pressed.
bool Slider::_collisionSwitchesReleased()
{
    return !_leftCollisionSwitchPressed() && !_rightCollisionSwitchPressed();
}

// The target position and the step length will be clamped to match the range of
// the slider determined by calibration.
void Slider::setTargetPosition(int targetPosition, float stepLength)
{
    // Clamp the target position to fit the slider range.
    _targetPosition = max(0, min(targetPosition, _sliderRange));
    // Clamp the step length to fit the physical reality of the slider.
    // The direction is not determined by this value's sign.
    _stepLength = max(0.2, min(abs(stepLength), (float)_sliderRange / 2.0));
    _remainder = 0.0;
}

// Returns the current position of the slider.
int Slider::position()
{
    return _position;
}

// Returns the target position of the slider.
int Slider::targetPosition()
{
    return _targetPosition;
}

// Returns the current step size of the slider.
float Slider::stepLength()
{
    return _stepLength;
}

// Move the motor to get closer to the target position.
//
// Returns true if there will be next move to reach the target position. Returns
// false if the position is close enough to the target position. Returns false
// also when the slider hits the collision switch.
void Slider::update()
{
    // Calculate how many steps should the motor move in this iteration,
    // including the remaining steps from the previous iterations.
    float currentSteps = _remainder + _stepLength;
    // The motor can only move whole number of steps. Calculate how many steps
    // can it move in this iteration.
    int actualSteps = (int)floor(currentSteps);
    // Remember how many steps couldn't the motor move in this iteration (should
    // be < 1.0).
    _remainder = currentSteps - actualSteps;
    // If the motor should move to the left, make this negative to move the
    // motor the opposite direction.
    if (_position > _targetPosition)
    {
        actualSteps = -actualSteps;
    }
    // If not pushing any collision switch
    if (_collisionSwitchesReleased())
    {
        if (!isAtTarget())
        {
            // move the actual motor.
            _motor.step(actualSteps);
            // Remember the actual position.
            _position += actualSteps;
        }
    }
    else
    {
        // The slider appears to be uncalibrated. Don't move and set the flag.
        _calibrated = false;
    }
}

bool Slider::_leftCollisionSwitchPressed()
{
    bool pressed = digitalRead(_leftSwitchPin) == LOW;
    if (pressed)
    {
        Serial.println(">>>> Left collision switch pressed.");
    }

    return pressed;
}

bool Slider::_rightCollisionSwitchPressed()
{
    bool pressed = digitalRead(_rightSwitchPin) == LOW;
    if (pressed)
    {
        Serial.println(">>>> Right collision switch pressed.");
    }

    return pressed;
}

// True if the slider is close enough to the target.
bool Slider::isAtTarget()
{
    return abs(_position - _targetPosition) < _stepLength;
}

// Calibrate the slider. This function is about to be called by the Arduino loop
// while isCalibrated() is false. The calibration doesn't stop the rest of the
// Arduino program.
void Slider::calibrationUpdate()
{
    // Only calibrate if needed
    if (!_calibrated)
    {
        // First calibrate left side, then right
        if (_calibrationDirection)
        {
            // Calibrating right side
            if (!_rightCollisionSwitchPressed())
            {
                // If not touching the right collision switch, move one step to the
                // right
                _motor.step(1);
                // and count the position.
                _position++;
            }
            else
            {
                // Once the right collision switch has been pressed, set the maximum
                // step count of the slider to the current position.
                _sliderRange = _position;
                // Switch the calibration direction if another calibration will be
                // needed.
                _calibrationDirection = false;
                // Mark the slider calibrated.
                _calibrated = true;
                // Move the slider back a little so that it's not touching the
                // collision switch anymore.
                while (_rightCollisionSwitchPressed())
                {
                    _motor.step(-1);
                }
            }
        }
        else
        {
            // Calibrating left side
            if (!_leftCollisionSwitchPressed())
            {
                // If not touching the left collision switch, move one step to the
                // left.
                _motor.step(-1);
                // Count the current position although this value may  be
                // faulty. It's important to do so, if the slider calibration is
                // only slightly off and the calibration is interrupted to
                // continue with normal update.
                _position--;
            }
            else
            {
                // Once the left collision switch has been pressed, set the current
                // position to zero
                _position = 0;
                // and change calibration direction.
                _calibrationDirection = true;
                // Move the slider back a little so that it's not touching the
                // collision switch anymore.
                while (_leftCollisionSwitchPressed())
                {
                    _motor.step(1);
                }
            }
        }
    }
}

// Returns the maximum position for the slider determind by the calibration.
// Minimum is always 0.
int Slider::sliderRange()
{
    return _sliderRange;
}

// Returns true if the system has been calibrated and no discrepancy has been
// found during normal use.
bool Slider::isCalibrated()
{
    return _calibrated;
}

//  █████╗ ██████╗ ██████╗ ██╗   ██╗██╗███╗   ██╗ ██████╗
// ██╔══██╗██╔══██╗██╔══██╗██║   ██║██║████╗  ██║██╔═══██╗
// ███████║██████╔╝██║  ██║██║   ██║██║██╔██╗ ██║██║   ██║
// ██╔══██║██╔══██╗██║  ██║██║   ██║██║██║╚██╗██║██║   ██║
// ██║  ██║██║  ██║██████╔╝╚██████╔╝██║██║ ╚████║╚██████╔╝
// ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝  ╚═════╝ ╚═╝╚═╝  ╚═══╝ ╚═════╝

Slider slider1(
    // Steps per revolution
    2048,
    // Stepper motor pins
    8, 10, 9, 11,
    // Left and right collision switch pins
    2, 3);

void setup()
{
    Serial.begin(9600);
    Serial.println("Program start");
    // First calibrate the slider
    while (!slider1.isCalibrated())
    {
        slider1.calibrationUpdate();
    }
    Serial.println("Done calibrating.");
    Serial.print("Current position is ");
    Serial.println(slider1.position());
    // Set the current position as target to prevent the slider from moving at
    // the beginning.
    slider1.setTargetPosition(slider1.position(), 1.0);
    Serial.print("Slider range is ");
    Serial.println(slider1.sliderRange());
    Serial.print("Target position is ");
    Serial.println(slider1.targetPosition());
}

void loop()
{
    if (slider1.isCalibrated())
    {
        // If the slider is calibrated, it can be moved.
        if (slider1.isAtTarget())
        {
            // If it already is at its target, change the target to something
            // random.
            int newTarget = random(slider1.sliderRange());
            // Random returns long, desired random range is 0.2 to 3.0
            float newStepLength = random(20, 300) / 100.0;
            slider1.setTargetPosition(newTarget, newStepLength);
            Serial.print("> Current position is ");
            Serial.println(slider1.position());
            Serial.print("> Target position is ");
            Serial.println(slider1.targetPosition());
            Serial.print("> Step length is ");
            Serial.println(slider1.stepLength());
        }
        else
        {
            // If not at the target, then move towards it.
            slider1.update();
        }
    }
    else
    {
        // Perform calibration update if needed.
        slider1.calibrationUpdate();
    }
}
